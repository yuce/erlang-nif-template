#! /usr/bin/env sh

ERL_PATH_FILE=".erl-path"

dirname=$(dirname $0)
build_dir="${dirname}/_build"
if [ -d "${build_dir}" ]; then
    ERL_PATH_FILE="${build_dir}/${ERL_PATH_FILE}"
fi

if [ -f "${ERL_PATH_FILE}" ]; then
    cat ${ERL_PATH_FILE}
    exit 0
fi

erl_path=$(erl -eval 'io:format("~s", [lists:concat([code:root_dir(), "/erts-", erlang:system_info(version)])])' -s init stop -noshell)
echo ${erl_path} > ${ERL_PATH_FILE}
echo ${erl_path}
