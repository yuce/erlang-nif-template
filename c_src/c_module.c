/*
    Author: {{.author}}
    Created on: {{.date}}, at: {{.time}}
*/
{{- if .license}}
/*
{{- range .license}}
    {{.}}
{{- end}}
*/
{{- end}}
{{- $module := or .module .filename }}

#include "erl_nif.h"


// Called when the NIF library is loaded and no previously loaded library exists for this module.
// See: http://erlang.org/doc/man/erl_nif.html#load
static int load(ErlNifEnv* env, void** priv, ERL_NIF_TERM load_info)
{
    return 0;
}

// Called when the NIF library is loaded and there is old code of this module with a loaded NIF library.
// See: http://erlang.org/doc/man/erl_nif.html#upgrade
static int upgrade(ErlNifEnv* env, void** priv, void** old_priv, ERL_NIF_TERM load_info)
{
    return 0;
}

// Called when the module code that the NIF library belongs to is purged as old.
// New code of the same module may or may not exist.
// See: http://erlang.org/doc/man/erl_nif.html#unload
static void unload(ErlNifEnv* env, void* priv)
{
    return;
}

static ERL_NIF_TERM sum(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
    int a, b;

    if(argc != 2) {
        return enif_make_badarg(env);
    }

    if (!enif_get_int(env, argv[0], &a)) {
        return enif_make_badarg(env);
    }

    if (!enif_get_int(env, argv[1], &b)) {
        return enif_make_badarg(env);
    }

    return enif_make_int(env, a + b);
}

static ErlNifFunc nif_funcs[] = {
    // {erl_function_name, erl_function_arity, c_function, flags}
    {"sum", 2, sum, 0}
};

ERL_NIF_INIT({{$module}}, nif_funcs, &load, NULL, &upgrade, &unload)
