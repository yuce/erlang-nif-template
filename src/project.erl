%%
%% Author: {{.author}}
%% Created on: {{.date}}, at: {{.time}}
{{- if .license}}
%%
{{- range .license}}
%% {{.}}
{{- end}}
{{- end}}
%%
{{- $project := or .project .target_dir }}

-module({{.filename}}).

-export([sum/2]).

-on_load(init/0).

%% === API functions ===

sum(_A, _B) ->
    exit(nif_library_not_loaded).

%% === Internal functions ===

init() ->
    ok = erlang:load_nif("./priv/{{$project}}", 0).