require path
{{- $project := to_lower (or .project .target_dir)}}

# project files
mk src/{{$project}}.erl -t src/project.erl project={{$project}}
mk src/{{$project}}.app.src -t src/project.app.src project={{$project}}
mk rebar.config -t rebar.config

mk c_src/{{$project}}.c -t c_src/c_module.c
mk -F c_src/Makefile -t c_src/Makefile
mk erl_path.sh -c erl_path.sh
mk priv

# other files
mk README.md
mk .gitignore -t erlang
mk -F LICENSE {{if .license_name}}-t {{.license_name}}{{end}}